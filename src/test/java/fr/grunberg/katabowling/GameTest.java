package fr.grunberg.katabowling;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class GameTest {
	@Test
	public void singleShotSimple() {
		Game game = new Game();
		game.roll(8);
		assertEquals(8, game.score());
	}

	@Test
	public void singleFrameSimple() {
		Game game = new Game();
		game.roll(8);
		game.roll(1);
		assertEquals(9, game.score());
	}

	@Test
	public void singleFrameSpare() {
		Game game = new Game();
		game.roll(8);
		game.roll(2);
		assertEquals(10, game.score());
	}

	@Test
	public void twoFramesSpare() {
		Game game = new Game();
		game.roll(8);
		game.roll(2);
		game.roll(6);
		game.roll(1);
		assertEquals(23, game.score());
	}
	@Test
	public void twoFramesStrike() {
		Game game = new Game();
		game.roll(10);
		game.roll(6);
		game.roll(1);
		assertEquals(24, game.score());
	}
	@Test
	public void threeFramesStrikeOnSecond() {
		Game game = new Game();
		game.roll(4);
		game.roll(0);
		game.roll(10);
		game.roll(6);
		game.roll(1);
		assertEquals(28, game.score());
	}
	@Test
	public void threeFramesStrikeUnfinished() {
		Game game = new Game();
		game.roll(4);
		game.roll(0);
		game.roll(10);
		game.roll(0);
		assertEquals(14, game.score());
	}
	@Test
	public void bestGameEver() {
		Game game = new Game();
		game.roll(10);
		game.roll(10);
		game.roll(10);
		game.roll(10);
		game.roll(10);
		game.roll(10);
		game.roll(10);
		game.roll(10);
		game.roll(10);
		game.roll(10);
		game.roll(10);
		game.roll(10);
		assertEquals(300, game.score());
	}
}
