package fr.grunberg.katabowling;

class Frame {
	private int scoreFirstThrow = -1;
	private int scoreSecondThrow = -1;
	private Frame nextFrame;

	private final boolean bonusFrame;
	
	public Frame(boolean bonusFrame) {
		this.bonusFrame = bonusFrame;
	}
	
	public Frame() {
		this(false);
	}
	
	public void addScore(int pinsDown) {
		if(scoreFirstThrow == -1) {
			scoreFirstThrow = pinsDown;
		}
		else if(scoreSecondThrow == -1) {
			scoreSecondThrow = pinsDown;
			}
		else {
			throw new IllegalStateException("Frame already done.");
		}
	}
	public boolean isComplete() {
		if(scoreFirstThrow == -1)
			return false;
		if(scoreSecondThrow != -1)
			return true;
		if(scoreFirstThrow == 10 || scoreFirstThrow + scoreSecondThrow == 10)
			return true;
		return false;
	}
	public boolean isStrike() {
		return scoreFirstThrow == 10;
	}
	public boolean isSpare() {
		return scoreFirstThrow + scoreSecondThrow == 10;
	}
	private int getRawScore() {
		return 
				(scoreFirstThrow == -1 ? 0 : scoreFirstThrow)
		+		(scoreSecondThrow == -1 ? 0 : scoreSecondThrow);
	}
	
	public int getTotalScore() {
		if(bonusFrame) {
			return 0;
		}
		if(isStrike()) {
			if(nextFrame != null) {
				if(nextFrame.getScoreFirstThrow() == -1) {
					return 10;
				}
				if(nextFrame.isStrike()) {
					if(nextFrame.getNextFrame() != null && nextFrame.getNextFrame().isComplete()) {
						return 10 + 10 + nextFrame.getNextFrame().getScoreFirstThrow();
					}
					return 10 + 10;
				}
				if(nextFrame.getScoreSecondThrow() > -1) {
					return 10 + nextFrame.getScoreFirstThrow() + nextFrame.getScoreSecondThrow();
				}
				else {
					return 10 + nextFrame.getScoreFirstThrow();
				}
			}
			return 10;
		}
		else if(isSpare()) {
			if(nextFrame != null)
				return 10 + (nextFrame.getScoreFirstThrow() > -1 ? nextFrame.getScoreFirstThrow(): 0);
			return 10;
		}
		else {
			return getRawScore();
		}
	}
	public int getScoreFirstThrow() {
		return scoreFirstThrow;
	}
	public int getScoreSecondThrow() {
		return scoreSecondThrow;
	}
	public Frame getNextFrame() {
		return nextFrame;
	}
	public void setNextFrame(Frame nextFrame) {
		this.nextFrame = nextFrame;
	}
}
