package fr.grunberg.katabowling;

import java.util.ArrayList;
import java.util.List;

public class Game {
	private final List<Frame> frames = new ArrayList<>(12);
	
	public void roll(int pinsDown) {
		if(frames.isEmpty()) {
			frames.add(new Frame());
		}
		if(frames.get(frames.size()-1).isComplete()) {
			frames.add(new Frame(frames.size()>=10));
			frames.get(frames.size()-2).setNextFrame(frames.get(frames.size()-1));
		}
		Frame targetFrame = frames.get(frames.size()-1);
		targetFrame.addScore(pinsDown);
	}
	
	public int score() {
		return frames.stream()
				.mapToInt(f -> f.getTotalScore())
				.sum();
	}
}
